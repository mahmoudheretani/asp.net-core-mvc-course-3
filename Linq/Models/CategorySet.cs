﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class CategorySet:BaseEntity
    {
        public string Name { get; set; }

        public ICollection<SubCategorySet> SubCategories { get; set; }
    }
}
