﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Dto
{
    public class CategoryInfoDto
    {
        public int Id { get; set; }

        public int Count { get; set; }
        public IEnumerable<SubCategoryInfoDto> SubCategories { get; set; }

    }
}
