﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool IsValid { get; set; }
    }
}
