﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class LaptopItemSet
    {
        public double Price { get; set; }

        public int LaptopId { get; set; }
        public LaptopSet Laptop { get; set; }

        public int ItemId { get; set; }
        public ItemSet Item { get; set; }
    }
}
