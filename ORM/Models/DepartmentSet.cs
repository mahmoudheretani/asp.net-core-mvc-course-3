﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class DepartmentSet
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int EmployeesCount { get; set; }

        public ICollection<CourseSet> Courses { get; set; }
        public ICollection<EmployeeSet> Employees { get; set; }
    }
}
