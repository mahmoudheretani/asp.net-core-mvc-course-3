﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.Models
{
    public class BookSet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset PublishDate { get; set; }
    }
}
