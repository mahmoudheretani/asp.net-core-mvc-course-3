﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DI.Exporter
{
    public interface IExporter
    {
        string ExportData(List<string> data);
    }
}
