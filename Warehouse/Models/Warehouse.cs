﻿using LINQToEntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LINQToEntityFramework.Models
{
    public class WarehouseSet : BaseEntity
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }

        public ICollection<BookWarehouse> BookWarehouses { get; set; }

        public WarehouseSet()
        {
            BookWarehouses = new HashSet<BookWarehouse>();
        }
    }
}
