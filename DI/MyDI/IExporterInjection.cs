﻿using DI.Exporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DI.MyDI
{
    public interface IExporterInjection
    {
         void InjectExporter(IExporter exporter);
    }
}
