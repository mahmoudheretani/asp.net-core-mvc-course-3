﻿using LINQToEntityFramework.Models;
using LINQToEntityFramework.Repositories;
using LINQToEntityFramework.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Warehouse.Repositories
{
    public class WarehouseRepository : IWarehouseRepository
    {
        private WarehouseDbContext Context { get; }
        public WarehouseRepository(WarehouseDbContext warehouseDbContext)
        {
            Context = warehouseDbContext;
        }
        public IEnumerable<Order> GetAllCustomerOrders(int customerId)
        {
            return Context.Orders.Where(order => order.CustomerId == customerId).ToList();
        }

        public IEnumerable<WarehouseSet> GetAllWarehourses(string query)
        {
            return Context.Warehouses
                .Where(warehouse => string.IsNullOrEmpty(query) || warehouse.Name.Contains(query))
                 .ToList();
        }

        public IDictionary<Author, IEnumerable<Book>> GetAuthorsBook()
        {
            return Context.Authors
                .ToDictionary(author => author, author => author.Books.AsEnumerable());
        }

        public double GetAuthorTotalSalles(int authorId)
        {
            throw new NotImplementedException();
        }

        public int GetBookCopiesCount(int bookId)
        {
            return Context.BookWarehouses
                .Where(bookWarehouse => bookWarehouse.BookId == bookId)
                .Sum(bookWarehouse => bookWarehouse.Amount);
        }

        public double GetBookCopiesPrice(int bookId)
        {
            Context.Books
                .Where(book => book.Id == bookId)
                .Select(book =>
                                book.Price 
                                * 
                                book.BookWarehouses
                                        .Sum(bookwarehouse => bookwarehouse.Amount)
                        );
        }

        public IDictionary<string, int> GetBooksTotalSallesCopies()
        {
            throw new NotImplementedException();
        }

        public Order GetLatestOrder()
        {
            return Context.Orders
                .OrderByDescending(order => order.DateTime)
                .FirstOrDefault();
        }

        public double GetOrderValuesForMonth(int month, int year)
        {
            return Context.Orders
                .Where(order => 
                    order.DateTime.Year == year 
                    && 
                    order.DateTime.Month == month
                ).Sum(order=>order.BooksCount*order.Book.Price);
        }

        public IEnumerable<WarehouseSet> GetPublisherWarehourses(int publisherId)
        {
            return Context.
        }

        public int GetTotalBooksCount()
        {
            return Context.Books.Count();
        }
    }
}
