﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class TrafficSet:BaseEntity
    {
        public int Amount { get; set; }
        public int BookStoreId { get; set; }
        public BookStoreSet BookStore { get; set; }
    }
}
