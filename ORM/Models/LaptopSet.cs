﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class LaptopSet
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public ICollection<LaptopItemSet> LaptopItems { get; set; }

    }
}
