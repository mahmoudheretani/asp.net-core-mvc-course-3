﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class AddressSet
    {
        public int Id { get; set; }
        public string Street { get; set; }

        public int? PersonId { get; set; }
        public PersonSet Person { get; set; }
    }
}
