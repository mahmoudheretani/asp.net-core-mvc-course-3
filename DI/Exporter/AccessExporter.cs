﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DI.Exporter
{
    public class AccessExporter : IExporter
    {
        public string ExportData(List<string> data)
        {
            return data[0] + "From access";
        }
    }
}
