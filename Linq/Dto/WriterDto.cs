﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Dto
{
    //Data transfare object
    public class WriterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
