﻿using CourseProject.ViewModels.ViewComponents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewComponents
{
    public class UserViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke(string username, int location)
        {
            var vm = new UserViewModel()
            {
                Location= location,
                Username= username
            };
            return View(vm);
        } 
    }
}
