﻿using Linq.Database;
using Linq.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static IEnumerable<WriterDto> GetWritersName()
        {
            using(var context=new BookDbContext())
            {
               return context.Writers
                    .Select(writer =>new WriterDto()
                    {
                        Id=writer.Id,
                        Name=writer.Name
                    })
                    .ToList();
            }
        }

        public static void SelectMany()
        {
            using (var context = new BookDbContext())
            {
                var result= context.Categories
                    .Where(cat => cat.IsValid && !string.IsNullOrEmpty(cat.Name))
                    .SelectMany(cat => cat.SubCategories)
                    .ToList(); 
            }
        }

        public static void SkipTake(int pageSize,int pageNumber)
        {
            using (var context = new BookDbContext())
            {
                var query = context.Books.Where(book => book.IsValid)
                    .OrderBy(book => book.Title);
                var totalResultsCount = query.Count();

                var finalData= query.Skip(pageSize * pageNumber)
                    .Take(pageSize)
                    .ToList();
            }
        }

        public static void ToDictionary()
        {
            using (var context = new BookDbContext())
            {
                var subCatBooks = context.SubCategories
                     .ToDictionary(subCat => subCat.Name, subCat => subCat.Books);
            }
        }

        public static void ToDictionaryUsingDto()
        {
            using (var context = new BookDbContext())
            {
                var subCatBooks = context.SubCategories
                    .Select(subCat => new SubCategoryInfoDto()
                    {
                        Id=subCat.Id,
                        Books=subCat.Books.Select(book=>new BookDto()
                        {
                            Id=book.Id,
                            Name=book.Title
                        }).ToList()
                    }).ToList();
                     
            }
        }


        public static void GetCategoriesFullInfo()
        {
            using (var context = new BookDbContext())
            {
                var subCatBooks = context.Categories
                    .Select(cat=>new CategoryInfoDto()
                    {
                        Id=cat.Id,
                        Count=cat.SubCategories.Count,
                        SubCategories=cat.SubCategories
                        .Select(subCat => new SubCategoryInfoDto()
                        {
                            Id = subCat.Id,
                            Books = subCat.Books.Select(book => new BookDto()
                            {
                                Id = book.Id,
                                Name = book.Title
                            }).ToList()
                        }).ToList()
                    }).ToList();

            }
        }


        public static async void Async()
        {
            using (var context = new BookDbContext())
            {
                var books = context.Books.ToList();
                var booksAsync =await context.Books.ToListAsync();
            }
        }
        public static void Include()
        {
            using (var context = new BookDbContext())
            {
                var categories = context.Categories
                    .Where(cat=>cat.IsValid && cat.SubCategories.Count>0)
                    .ToList();
                foreach (var category in categories)
                {
                    Console.WriteLine(category.SubCategories.Count);
                }

                 categories = context.Categories
                    .Include(cat=>cat.SubCategories)
                    .ThenInclude(subCat=>subCat.Books)
                  //.Where(cat => cat.IsValid && cat.SubCategories.Count > 0)
                  .ToList();
                foreach (var category in categories)
                {
                    Console.WriteLine(category.SubCategories.Count);
                }
            }
        }



        public static void GetBooks()
        {
            using (var context = new BookDbContext())
            {
                var books= context.Books
                     .Where(book=>book.IsValid 
                                    && 
                                    book.Price>1000 
                                    && 
                                    book.Title.Contains("A")
                            )
                     .Select(book=>book.Title)
                     .ToList();
            }
        }

        public static bool DoesStoreHasBook(int storeId,int bookId)
        {
            using (var context = new BookDbContext())
            {
              return  context.BookStores
                    .Any(bookStore => bookStore.StoreId == storeId
                                    &&
                                    bookStore.BookId == bookId
                                    );
                 //context.BookStores
                 //   .Where(bookStore => bookStore.StoreId == storeId
                 //                   &&
                 //                   bookStore.BookId == bookId
                 //                   ).Select(bookStore=>bookStore.BookId;
            }
        }

        public static void FirstLastSingle(int storeId, int bookId)
        {
            using (var context = new BookDbContext())
            {
                var bookWithA = context.Books
                      .Where(book => book.Title.Contains("A"))
                      .Select(book => new { book.Title, book.PagesCount })
                      .LastOrDefault();
            }
        }

        public static int Math()
        {
            using (var context = new BookDbContext())
            {
                var booksNumber = context.Books.Count();
                var validBooks = context.Books.Count(book=>book.IsValid);

                var sumOfPrices = context.Books.Sum(book => book.Price);
                var avgOfPrices = context.Books.Average(book => book.Price);
                var maxOfPrices = context.Books.Max(book => book.Price);
                var minOfPrices = context.Books.Min(book => book.Price);


                return validBooks;
            }
        }

        public void OrderedValidBooks()
        {
            using (var context = new BookDbContext())
            {
                var result = context.Books.Where(book => book.IsValid)
                    .OrderByDescending(book => book.Price)
                    .ThenBy(book=>book.Title)
                    .ToList();
            }
        }

    }
}
