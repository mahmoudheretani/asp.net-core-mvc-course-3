﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class BookStoreSet:BaseEntity
    {

        public int BookId { get; set; }
        public BookSet Book { get; set; }

        public int StoreId { get; set; }
        public StoreSet Store { get; set; }

         public ICollection<TrafficSet> Traffics { get; set; }

    }
}
