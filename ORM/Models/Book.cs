﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ORM.Models
{
    [Table("BookTable",Schema ="Library")]
    public class BookSet
    {
        [Key]
        public int ISBN { get; set; }
        [Column("BookTitle")]
        [Required]
        public string Title { get; set; } = "test";

        [StringLength(1000,MinimumLength =50)]
        public string Description { get; set; }

        [NotMapped]
        public double Price { get; set; }

        public string BriefText=> Title + " " + Description.Substring(0, 75);
    }
}
