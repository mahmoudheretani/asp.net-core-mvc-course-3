﻿using DI.Exporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DI.MyDI
{
    public class CreatorDI:IDependenceyInjection
    {
        public IExporter CreateAccessExporter()
        {
            return new AccessExporter();
        }

        public IExporter CreateMySqlExporter()
        {
            return new MySqlExporter();
        }

        public IExporter ExportPdf()
        {
            return new PdfExporter();
        }
    }
}
