﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Dto
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
