﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class PersonSet
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public AddressSet Address { get; set; }
    }
}
