﻿using CourseProject.Util;
using CourseProject.ViewModels.Product;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.Controllers
{
    public class ProductController:Controller
    {
        public IActionResult Index()
        {
            var vm = new IndexViewModel()
            {
                ProductCardsData = GetDummyData()
            };
            return View(vm);
        }

        public IActionResult ProductCard()
        {
            return PartialView(ViewsPaths._ProductCard, GetDummyData()[0]);
        }

        public IActionResult View(int id)
        {
            return Content(id.ToString());
        }

        public IActionResult Search()
        {
            var vm = new SearchViewModel()
            {
                ProductCardsData = GetDummyData()
            };
            return View(vm);
        }

        private List<ProductCardViewModel> GetDummyData()
        {
            return new List<ProductCardViewModel>()
            {
                new ProductCardViewModel()
                {
                    Id=1,
                    ImageUrl="/images/image.jpg",
                    OfferedDate=new DateTimeOffset(new DateTime(2019,8,8)),
                    Price=200,
                    Title="New Product 1"
                },
                new ProductCardViewModel()
                {
                    Id=2,
                    ImageUrl="/images/image.jpg",
                    OfferedDate=new DateTimeOffset(new DateTime(2019,7,8)),
                    Price=150,
                    Title="New Product 2"
                }
            };
        }
    }
}
