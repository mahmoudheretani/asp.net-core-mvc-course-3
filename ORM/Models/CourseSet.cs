﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ORM.Models
{
    [Table("Courses", Schema = "Education")]
    public class CourseSet
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int MaxNumberOfPeople { get; set; }
        public int MinNumberOfPeople { get; set; }
        public int? DepartmentId { get; set; }
        public DepartmentSet Department { get; set; }
    }
}
