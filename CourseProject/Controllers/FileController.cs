﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CourseProject.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CourseProject.Controllers
{
    public class FileController : Controller
    {
        private IHostingEnvironment HostingEnvironment { get;  }
        public FileController(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult DownloadFile()
        {
            var filePath = "data/3.pdf";//file url
            return ControllerExtensions.DownloadFile
                (this, HostingEnvironment.WebRootPath, filePath, "new file.pdf", "application/pdf");
        }

    }
}
