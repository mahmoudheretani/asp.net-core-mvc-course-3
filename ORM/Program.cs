﻿using Microsoft.EntityFrameworkCore;
using ORM.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ORM
{
    class Program
    {
        static void Main(string[] args)
        {
            //var context = new OfficeDbContext();
            //var data= context.Departments.ToList();
            //foreach (var departmentEntity in data)
            //{
            //    Console.WriteLine(departmentEntity.Name);
            //}
            InsertDepartmentWithCourses();
        }

        public static void UpdateDepartment()
        {
            //connected 
            //using (var context = new OfficeDbContext())
            //{
            //    var testDep = context.Departments.Find(1);
            //    testDep.Name = "Updated name";
            //    context.SaveChanges();
            //}

            //disconnected
            using (var context = new OfficeDbContext())
            {
                //var testDep = context.Departments.Find(1);
                // testDep.Name = "Updated name";
                //context.SaveChanges();
                var updatedDep = new DepartmentSet()
                {
                    Id = 2
                };
                context.Departments.Attach(updatedDep);
                updatedDep.EmployeesCount = 2;
                context.SaveChanges();
            }
        }

        public static void DeleteDepartment()
        {
            //connected
            //using (var context = new OfficeDbContext())
            //{
            //    var testDep = context.Departments.Find(1);
            //    context.Remove(testDep);
            //    context.SaveChanges();
            //}

            //disconnected
            using (var context = new OfficeDbContext())
            {
                var updatedDep = new DepartmentSet()
                {
                    Id = 2
                };
               // context.Departments.Attach(updatedDep);
                context.Entry(updatedDep).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
        public void InsertDepartment()
        {
            using(var context=new OfficeDbContext())
            {
                var department = new DepartmentSet()
                {
                    Name="New department"
                };
                var department2 = new DepartmentSet()
                {
                    Name = "New department"
                };
                context.AddRange(department, department2);
                //context.Add(department);
                //context.Add<DepartmentSet>(department);
                //context.Departments.Add(department);
                //context.Entry(department).State = EntityState.Added;
                context.SaveChanges();
            }
        }


        public static void InsertDepartmentWithCourses()
        {
            using(var context=new OfficeDbContext())
            {
                var newITDep = new DepartmentSet()
                {
                    Name = "IT",
                    EmployeesCount = 50
                };
                newITDep.Courses = new List<CourseSet>()
                {
                    new CourseSet()
                    {
                        Title="C# course",
                        MaxNumberOfPeople=10,
                        MinNumberOfPeople=5
                    }
                };
                context.Add(newITDep);
                context.SaveChanges();
            }
        }

    }
}
