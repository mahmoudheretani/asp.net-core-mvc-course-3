﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class WriterSet:BaseEntity
    {
        public string Name { get; set; }
        public DateTimeOffset Birthdate { get; set; }
        public ICollection<BookSet> Books { get; set; }
    }
}
