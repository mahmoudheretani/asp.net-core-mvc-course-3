﻿using DI.MyDI;
using System;

namespace DI
{
    class Program
    {
        /// <summary>
        /// MVC
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var creatorDI = new CreatorDI();
            // var myApp = new App(creatorDI.CreateMySqlExporter());
             var myApp = new App(new CreatorDI());
            //myApp.Exporter = creatorDI.CreateMySqlExporter();
            //myApp.InjectExporter(creatorDI.CreateAccessExporter());
            Console.WriteLine("Hello World!");
        }
    }
}
