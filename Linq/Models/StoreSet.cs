﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class StoreSet:BaseEntity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public ICollection<BookStoreSet> BookStores { get; set; }

    }
}
