﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewModels.ViewComponents
{
    public class UserViewModel
    {
        public string Username { get; set; }
        public int Location { get; set; }
    }
}
