﻿using CourseProject.Models;
using CourseProject.ViewModels.Book;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject.Controllers
{
    public class BookController : Controller
    {

        public BookController()
        {

        }

        public IActionResult Index()
        {
            //var books = GetBookList();
            //ViewData["booksList"] = books;
            //return View();

            var vm = new IndexViewModel()
            {
                Books=GetBookList()
            };
            return View(vm);
        }

        private List<BookSet> GetBookList()
        {
            return new List<BookSet>()
            {
                new BookSet()
                {
                    Id=1,
                    Name="Getting started with mvc",
                    PublishDate=new DateTimeOffset(new DateTime(2017,1,1))
                },
                new BookSet()
                {
                    Id=2,
                    Name="Getting started with c#",
                    PublishDate=new DateTimeOffset(new DateTime(2018,1,1))
                },
                 new BookSet()
                {
                    Id=3,
                    Name="Getting started with java",
                    PublishDate=new DateTimeOffset(new DateTime(2019,5,5))
                }
            };
        }

        public IActionResult GetBookData(int id)
        {
            return Content(id.ToString());
        }

        public IActionResult BookBill()
        {
            return View();
        }


        [Route("[Controller]/[Action]/{day}/{month}/{year}")]
        public IActionResult GetWeatherInfo(int day, int month, int year)
        {
            return Content($"Weather on {day} {month} {year} is sunny");
        }
        public IActionResult GetBookInfo(int id)
        {
            BookSet book = null;
            foreach (var bookItem in GetBookList())
            {
                if (bookItem.Id == id)
                {
                    book = bookItem;
                }
            }
            if (book == null)
            {
                return Content("Book not found");
            }
            return Content($"{book.Id} {book.Name} {book.PublishDate}");
        }

        public IActionResult GetBookAsJson()
        {
            var book = new BookSet()
            {
                Id=1,
                Name="JSON book",
                PublishDate=new DateTimeOffset(new DateTime(2010,1,1))
            };
            return Json(book);
        }

        public IActionResult BookPageAsContent()
        {
            return StatusCode((int)HttpStatusCode.Conflict);
        }
    }
}
