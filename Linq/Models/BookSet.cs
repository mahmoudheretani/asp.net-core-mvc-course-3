﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class BookSet:BaseEntity
    {
        public string Title { get; set; }
        public int PagesCount { get; set; }
        public double Price { get; set; }

        public string Description { get; set; }


        public int SubCategoryId { get; set; }
        public SubCategorySet SubCategory { get; set; }
        public int WriterId { get; set; }
        public WriterSet Writer { get; set; }

        public ICollection<BookStoreSet> BookStores { get; set; }
    }
}
