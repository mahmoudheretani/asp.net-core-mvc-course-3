﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class EmployeeSet
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentSet Department { get; set; }
    }
}
