﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Models
{
    public class SubCategorySet:BaseEntity
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }
        public CategorySet Category { get; set; }

        public ICollection<BookSet> Books { get; set; }

    }
}
