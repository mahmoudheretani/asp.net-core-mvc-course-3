﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace CourseProject.Controllers
{
    public class SubscriptionController : Controller
    {
        public IActionResult ExpiredUser(string username)
        {
            return Content($"Sorry ! {username}");
        }
    }
}
