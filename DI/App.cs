﻿using DI.Exporter;
using DI.MyDI;
using System;
using System.Collections.Generic;
using System.Text;

namespace DI
{
    public  class App//: IExporterInjection
    {
        //public IExporter Exporter { get; set; }

        private IDependenceyInjection DependenceyInjection { get; }
        public App(IDependenceyInjection dependenceyInjection)
        {
            DependenceyInjection = dependenceyInjection;
        }
        //public void InjectExporter(IExporter exporter)
        //{
        //    Exporter = exporter;
        //}

        //public App(IExporter exporter)
        //{
        //    Exporter = exporter;
        //}
        public void Run()
        {


            Console.WriteLine("App started");
            Console.ReadKey();
            Console.WriteLine("Exporting data ....");
            // var access = new AccessExporter();
            // Console.WriteLine(Exporter.ExportData(new List<string>() { "Data from database" }));
        //   Console.WriteLine(DependenceyInjection.().ExportData(new List<string>() { "Data from database" }));
        }


    }
}
