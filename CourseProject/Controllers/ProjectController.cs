﻿using CourseProject.Results;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.Controllers
{
    public class ProjectController : Controller
    {
        public IActionResult HelloWorld()
        {
            return Content("Hello world from ASP.NET Core MVC");
        }

        public IActionResult CustomHtml()
        {
            return BuildCustomHtmlResult("<h2>Hello world</h2>");
        }
        private CustomHtmlResult BuildCustomHtmlResult(string html)
        {
            return new CustomHtmlResult()
            {
                Content = html 
            };
        }
    }
}
