﻿using DI.Exporter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DI.MyDI
{
    public interface IDependenceyInjection
    {

        IExporter CreateAccessExporter();
        IExporter CreateMySqlExporter();


    }
}
