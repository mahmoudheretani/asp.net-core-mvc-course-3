﻿using CourseProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewModels.Book
{
    public class IndexViewModel
    {
        public List<BookSet> Books { get; set; }
        public List<AdSet> Ads { get; set; }
    }
}
