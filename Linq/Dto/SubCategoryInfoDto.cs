﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Dto
{
    public class SubCategoryInfoDto
    {
        public int Id { get; set; }

        public IEnumerable<BookDto> Books { get; set; }

    }
}
