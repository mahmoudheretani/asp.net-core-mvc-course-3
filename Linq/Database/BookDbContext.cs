﻿using Linq.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Database
{
    public class BookDbContext:DbContext
    {
        public DbSet<BookSet> Books { get; set; }
        public DbSet<BookStoreSet> BookStores { get; set; }
        public DbSet<CategorySet> Categories { get; set; }
        public DbSet<SubCategorySet> SubCategories { get; set; }
        public DbSet<StoreSet> Stores { get; set; }
        public DbSet<TrafficSet> Traffics { get; set; }
        public DbSet<WriterSet> Writers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=PRO10-107-0-NEW\PROSQL;Initial Catalog=BooksDb;Integrated Security=True");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
