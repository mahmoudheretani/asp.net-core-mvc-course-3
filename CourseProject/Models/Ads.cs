﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.Models
{
    public class AdSet
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
