﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Models
{
    public class ItemSet
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<LaptopItemSet> LaptopItems { get; set; }
    }
}
