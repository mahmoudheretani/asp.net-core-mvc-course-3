﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace CourseProject.Extensions
{
    public static class ControllerExtensions 
    {
        public static FileResult DownloadFile(Controller controller, string
  wwwroot, string filePath, string fileName, string contentType)
        {
            IFileProvider provider = new PhysicalFileProvider(wwwroot);
            IFileInfo fileInfo = provider.GetFileInfo(filePath);
            Stream readStream = fileInfo.CreateReadStream();
            return controller.File(readStream, contentType, fileName);
        }        public static string ToActionName(this string controllerName)
        {
            return controllerName.Replace("Controller", string.Empty);
        }
    }
}
