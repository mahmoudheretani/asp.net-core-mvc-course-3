﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewModels.Product
{
    public class IndexViewModel
    {
        public List<ProductCardViewModel> ProductCardsData { get; set; }
    }
}
