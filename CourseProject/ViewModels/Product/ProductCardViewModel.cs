﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewModels.Product
{
    public class ProductCardViewModel
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public DateTimeOffset OfferedDate { get; set; }
        public double Price { get; set; }
    }
}
