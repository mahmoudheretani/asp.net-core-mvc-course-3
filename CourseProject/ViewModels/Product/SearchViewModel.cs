﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProject.ViewModels.Product
{
    public class SearchViewModel
    {
        public List<ProductCardViewModel> ProductCardsData { get; set; }
    }
}
